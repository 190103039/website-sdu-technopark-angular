import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientPageComponent} from "./core";
import {AdminPageComponent} from "./layout/admin/admin-main-layout/admin-page.component";

const routes: Routes = [
  // {path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '', component: ClientPageComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./layout/client/client.module').then(m => m.ClientModule)
      }
    ]
  },
  {
    path: 'admin', component: AdminPageComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./layout/admin/admin.module').then(m => m.AdminModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
