import {NgModule} from "@angular/core";
import * as component from './index';
import {ClientRouterModule} from "./client-router.module";
import {CommonModule, NgOptimizedImage} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {AngularSvgIconModule} from "angular-svg-icon";
import {ContentComponent} from "./index";
import {MatNativeDateModule} from '@angular/material/core';
import {MatTabsModule} from "@angular/material/tabs";
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule} from "@angular/router";
import {CoreModule} from "../../core/core.module";
import { CarouselComponent, IvyCarouselModule } from "angular-responsive-carousel";
import { CarouselModule } from "ngx-bootstrap/carousel";
import { NgxSpinnerModule } from "ngx-spinner";
import { SharedModule } from "src/app/shared/shared.module";
import { GoogleSigninButtonDirective } from "@abacritt/angularx-social-login";
@NgModule({
  declarations: [
    component.ContentComponent,
    component.AboutComponent,
    component.ContactsComponent,
    component.PortfolioMainComponent,
    component.PortfolioOneComponent,
    component.PortfolioTwoComponent,
    component.PortfolioThreeComponent,
    component.PortfolioPostsComponent,
    component.FaqComponent,
    component.OurTeamComponent,
    component.PostViewDetailsComponent,
    component.NewsComponent,
    component.ContactsFormComponent,
    component.LoginComponent,
    component.RegisterComponent,
 
  ],
  imports: [
    ClientRouterModule,
    CommonModule,
    RouterModule,
    FormsModule,
    AngularSvgIconModule,
    MatNativeDateModule,
    MatTabsModule,
    TranslateModule,
    NgOptimizedImage,
    IvyCarouselModule,
    CarouselModule,
    NgxSpinnerModule,
    SharedModule,


  ],
  bootstrap: [ContentComponent],
})
export class ClientModule {
}
