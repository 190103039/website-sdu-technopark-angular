import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpService } from 'src/app/core/http/http.service';

@Component({
  selector: 'app-our-team',
  templateUrl: './our-team.component.html',
  styleUrls: ['./our-team.component.scss'],
})
export class OurTeamComponent implements OnInit {
  myData$: any[] = [];
  students: any[] = [];
  teachers: any[] = [];
  images: any[] = [];
  constructor(
    private httpClient: HttpService,
    private sanitizer: DomSanitizer
  ) {}


  ngOnInit() {}


  //   this.httpClient
  //     .getMethod('api/v1/team-info/list')
  //     .subscribe((data: any) => {
  //       this.students = data
  //         .filter((item: any) => item.type === 'STUDENT')
  //         .map((item: any) => ({
  //           ...item,
  //           photo: this.sanitizer.bypassSecurityTrustResourceUrl(
  //             `data:image/png;base64, ${item.photo}`
  //           ),
  //         }));

  //       console.log(this.students);

  //       this.teachers = data
  //         .filter((item: any) => item.type === 'EMPLOYEE')
  //         .map((item: any) => ({
  //           ...item,
  //           photo: this.sanitizer.bypassSecurityTrustResourceUrl(
  //             `data:image/png;base64, ${item.photo}`
  //           ),
  //         }));
  //     });
  // }
  // createSlides() {
  //   for (let i = 0; i < this.students.length; i += 4) {
  //     this.slides.push(this.students.slice(i, i + 4));
  //   }
  // }
  // prevSlide() {
  //   const currentIndex = this.getCurrentSlideIndex();
  //   const prevIndex =
  //     currentIndex === 0 ? this.slides.length - 1 : currentIndex - 1;
  //   this.currentImage = this.slides[prevIndex][0];
  // }
  // nextSlide() {
  //   const currentIndex = this.getCurrentSlideIndex();
  //   const nextIndex =
  //     currentIndex === this.slides.length - 1 ? 0 : currentIndex + 1;
  //   this.currentImage = this.slides[nextIndex][0];
  // }

  // getCurrentSlideIndex(): number {
  //   return this.slides.findIndex((slide) => slide.includes(this.currentImage));
  // }
}
