import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import * as $ from 'jquery';
import 'swiper';

import Swiper from 'swiper';
import * as Aos from 'aos';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements AfterViewInit , OnInit{
  private swiper: Swiper | null = null; // Declare a variable to store the Swiper instance

  constructor(private router: Router) { }
  ngOnInit(): void {



  (function($) {
    "use strict";
    $(".menu__icon").on("click", function () {
      $(this).closest(".menu").toggleClass("menu_state_open");
    });
  })(jQuery);

  new Swiper(".swiper-container", {
    resistanceRatio: 0.7,
    spaceBetween: 30,
    effect: "fade",
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: false,
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + (index + 1) + "</span>";
      },
    },
  });

  Aos.init();




  }


  ngAfterViewInit() {
    setTimeout(() => {
      (function($) {
        "use strict";
        $(".menu__icon").on("click", function () {
          $(this).closest(".menu").toggleClass("menu_state_open");
        });
      })(jQuery);

      new Swiper(".swiper-container", {
        resistanceRatio: 0.7,
        spaceBetween: 30,
        effect: "fade",
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: false,
          renderBullet: function (index, className) {
            return '<span class="' + className + '">' + (index + 1) + "</span>";
          },
        },
      });

      Aos.init();



    }, 1000);


}

}
