import { Component } from '@angular/core';
import { HttpService } from '../../../../core/http/http.service';
import { map } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-post',
  templateUrl: './news-post.component.html',
  styleUrls: ['./news-post.component.scss'],
})
export class NewsComponent {
  private getAllPostsUrl = '/api/v1/posts/list';
  private searchUrl ='/api/v1/posts/search';
  posts: any[] = [];
  search!: string;
  searchResults!: any[];
  loading: boolean = true;
  constructor(
    private httpClient: HttpService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.getData();
  }
  search1() {

  this.httpClient
    .getMethod(this.searchUrl).subscribe(
      (response: any) => {
        this.searchResults.push(response);
        console.log(this.searchResults +'jk')
      },
      (error) => {
        console.error(error);
      }
    );
  }
  public getData() {
    this.spinner.show();
    this.httpClient
      .getMethod(this.getAllPostsUrl)
      .pipe(
        map((posts: any[]) => {
          return posts.map((post: any) => {
            if (post.content && post.content.contentByte) {
              post.contentText = decodeURIComponent(
                escape(atob(post.content.contentByte))
              );
            }
            return post;
          });
        })
      )
      .subscribe((posts: any[]) => {
        this.spinner.hide();
        this.loading = false;
        console.log(posts);
        this.posts = posts;
      });
  }
}
