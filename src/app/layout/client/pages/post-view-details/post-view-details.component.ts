import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpService} from "../../../../core/http/http.service";
import {map} from "rxjs";

@Component({
  selector: 'app-post-view-details',
  templateUrl: './post-view-details.component.html',
  styleUrls: ['./post-view-details.component.scss']
})
export class PostViewDetailsComponent implements OnInit {
  private getPostUrl = '/api/v1/posts/'
  private readonly id: string | null;
  private isNumber: boolean = false;
  post$: any[] = [];

  constructor(private route: ActivatedRoute, private http: HttpService) {
    //todo we will have problem with null pointer exception
    // need to redirect Not found Page
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.isNumber = !isNaN(Number(this.id));

    this.http.getMethod(`${this.getPostUrl}${this.id}`).pipe(
      map((post) => {
        if (post.content) {
          post.contentText = decodeURIComponent(escape(atob(post.content)));
        }
        return post;
      })
    ).subscribe((post) => {
      this.post$.push(post);
    });
  }
}
