import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostViewDetailsComponent } from './post-view-details.component';

describe('PostViewDetailsComponent', () => {
  let component: PostViewDetailsComponent;
  let fixture: ComponentFixture<PostViewDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostViewDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PostViewDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
