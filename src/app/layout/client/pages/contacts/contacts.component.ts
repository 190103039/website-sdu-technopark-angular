import {HttpClient} from '@angular/common/http';
import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { ContactService } from './contact.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent {


  myData$: any[] = [];
  generalMyData$: any[] = [];

  constructor(private httpClient: HttpClient,
              public activatedRoute: ActivatedRoute,
              private contactService: ContactService) {

  }

  ngOnInit() {
    this.httpClient.get(`/api/v1/contacts`).subscribe((next: any) => {
        //@ts-ignore
        next.forEach((element: any) => {
          console.log(element)
          this.myData$.push(element);
          console.log(this.myData$[0].id)
        })
      }
    );
    this.httpClient.get("api/v1/general/website").subscribe((next: any) => {
      this.generalMyData$.push(next);
    })
  }


}
