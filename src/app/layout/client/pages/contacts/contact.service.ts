import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private contactMessagesUrl = 'api/v1/contact-messages';

  constructor(private http: HttpClient) { }

  sendContactForm(name: string, email: string, comment: string) {
    const body = { name, email, comment };
    console.log(body)
    return this.http.post(this.contactMessagesUrl, body);
  }
}
