import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioPostsComponent } from './portfolio-posts.component';

describe('PortfolioPostsComponent', () => {
  let component: PortfolioPostsComponent;
  let fixture: ComponentFixture<PortfolioPostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortfolioPostsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PortfolioPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
