import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SocialAuthService } from '@abacritt/angularx-social-login';
import { AuthService } from 'src/app/auth/_services/auth.service';
import { StorageService } from 'src/app/auth/_services/storage.service';

interface TokenResponse {
  access_token: string;
  expires_in: number;
  token_type: string;
  scope: string;
}

interface IProfile {
  email: string;
  family_name: string;
  given_name: string;
  id: number;
  locale: string;
  name: string;
  picture: string;
  verified_email: true;
}

declare const gapi: any;
@Component({
  selector: 'app-login',
  templateUrl:'./login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: any = {
    username: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  user: TokenResponse | null = null;
  profile: IProfile | null = null;
 
  userProfile: any;
  constructor(private http: HttpClient,private socialAuthService: SocialAuthService,private authService: AuthService, private storageService: StorageService) {}

  ngOnInit() {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
    }

  }

  login() {
    const clientId = '459773865241-s26a0bat2sp0mre6hmdqf7hurmvd6boq.apps.googleusercontent.com';
    const redirectUri = 'http://localhost:4200/   ';
    const responseType = 'token';
    const scope = 'openid profile email';

    const authUrl = ` https://accounts.google.com/o/oauth2/v2/auth/oauthchooseaccount?gsiwebsdk=3&client_id=459773865241-s26a0bat2sp0mre6hmdqf7hurmvd6boq.apps.googleusercontent.com&scope=openid%20profile%20email&redirect_uri=storagerelay%3A%2F%2Fhttp%2Flocalhost%3A3000%3Fid%3Dauth338803&prompt=select_account&response_type=token&include_granted_scopes=true&enable_serial_consent=true&service=lso&o2v=2&flowName=GeneralOAuthFlow`;
    https://accounts.google.com/o/oauth2/v2/auth/oauthchooseaccount?gsiwebsdk=3&client_id=459773865241-s26a0bat2sp0mre6hmdqf7hurmvd6boq.apps.googleusercontent.com&scope=openid%20profile%20email&redirect_uri=storagerelay%3A%2F%2Fhttp%2Flocalhost%3A3000%3Fid%3Dauth338803&prompt=select_account&response_type=token&include_granted_scopes=true&enable_serial_consent=true&service=lso&o2v=2&flowName=GeneralOAuthFlow
    window.location.href = authUrl;
  }

  logOut() {
    // Perform the Google logout logic here
    this.profile = null;
  }
  signIn(){
    gapi.auth2.getAuthInstance().signIn().then(() => {
      this.isLoggedIn = true;
      this.userProfile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    });
  }
  onSubmit(): void {
    const { username, password } = this.form;

    this.authService.login(username, password).subscribe({
      next: data => {
        this.storageService.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.storageService.getUser().roles;
        this.reloadPage();
      },
      error: err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    });
  }

  reloadPage(): void {
    window.location.reload();
  }

  fetchUserProfile() {
    if (this.user) {
      const accessToken = this.user.access_token;
      const headers = {
        Authorization: `Bearer ${accessToken}`,
        Accept: 'application/json'
      };

      this.http.get<IProfile>(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${accessToken}`, { headers }).subscribe(
        res => {
          this.profile = res;
        },
        err => {
          console.error(err);
        }
      );

      
    }
    


  }
}
