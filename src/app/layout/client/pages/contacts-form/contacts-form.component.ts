import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactService } from '../contacts/contact.service';

@Component({
  selector: 'app-contacts-form',
  templateUrl: './contacts-form.component.html',
  styleUrls: ['./contacts-form.component.scss']
})
export class ContactsFormComponent {
  name = '';
  email = '';
  comment = '';
  title='contact.button'

  disabled = true;
  constructor(private httpClient: HttpClient,
    public activatedRoute: ActivatedRoute,
    private contactService: ContactService) {

  }

  ngOnInit() {
  }

    onSubmit(e: Event) {
      this.contactService.sendContactForm(this.name, this.email, this.comment).subscribe(() => {

        this.title='contact.button1'
        console.log('Form submitted successfully');
        this.disabled = false;
      }, (error) => {
        console.error('Error submitting form:', error);
        this.disabled  = true;
      }, () => {
        console.log("complete")
        this.title ="Sent"
       
      });
    }
  }

