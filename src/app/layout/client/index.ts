import { from } from 'rxjs';

export * from './pages/content/content.component';
export * from './pages/about/about.component';
export * from './pages/contacts/contacts.component'
export * from './pages/portfolio_pages/portfolio-main/portfolio-main.component';
export * from './pages/portfolio_pages/portfolio-one/portfolio-one.component';
export * from './pages/portfolio_pages/portfolio-posts/portfolio-posts.component';
export * from './pages/portfolio_pages/portfolio-three/portfolio-three.component';
export * from './pages/portfolio_pages/portfolio-two/portfolio-two.component';
export * from './pages/news/news-post.component';
export * from './pages/post-view-details/post-view-details.component';
export * from './pages/faq/faq.component';
export * from './pages/our-team/our-team.component';
export * from './pages/contacts-form/contacts-form.component';
export * from './pages/login/login.component'
export * from './pages/register/register.component'
