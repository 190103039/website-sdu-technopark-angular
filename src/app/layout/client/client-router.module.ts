import { RouterModule, Routes } from '@angular/router';
import {
  AboutComponent,
  ContactsComponent,
  ContentComponent,
  FaqComponent,
  NewsComponent,
  PostViewDetailsComponent,
  OurTeamComponent,
  LoginComponent,
  RegisterComponent,
} from './index';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: ContentComponent,
  },
  {
    path: 'about',
    component: AboutComponent,
  },
  {
    path: 'contacts',
    component: ContactsComponent,
  },
  {
    path: 'news',
    component: NewsComponent,
  },
  {
    path: 'news/post/:id',
    component: PostViewDetailsComponent,
  },
  {
    path: 'faq',
    component: FaqComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientRouterModule {}
