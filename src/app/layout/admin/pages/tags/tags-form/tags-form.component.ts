import { Component, Input, OnInit, Inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {AdminApiService} from "../../../../../core/http/adminApi/admin-api.service";
@Component({
  selector: 'app-tags-form',
  templateUrl: './tags-form.component.html',
  styleUrls: ['./tags-form.component.css']
})
export class TagsFormComponent implements OnInit {
  @Input() product: any = {};
  tags: any = {};
  vendor: any = {};
  selectedFile: any;
  label = 'Создать';
  constructor(
    private api: AdminApiService,
    private toastr: ToastrService,
    public router: Router,
    public dialogRef: MatDialogRef<TagsFormComponent>,
    @Inject(MAT_DIALOG_DATA) public id: any
  ) {}
  loader: boolean = false;
  img_url = 'http://localhost:4200/assets/no-image-available.png';

  ngOnInit(): void {
    if (this.id) {
      this.label = 'Добавить';
      let data = {
        id: this.id,
      };
      this.api.TeamMemberById(data).subscribe((data: any) => {
        this.tags = data;
        // this.img_url = data.photo;
        console.log(this.tags);
      });
    }
    this.getTeamMember();
    this.getFile(this.selectedFile);
  }
  getFile(event: any) {
    if (event) {
      this.selectedFile = event.target.files[0];
      this.label = this.selectedFile.name;
      let reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        // this.img_url = event.target.result;
      };
    }
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);
  }

  SaveData() {
    if (this.id) {
      let formData = new FormData();
      formData.append('fullName', this.tags.fullName);
      formData.append('photo', this.selectedFile);
      formData.append('type', this.tags.type);
      this.loader = true;
      this.api.updateTeamMember(this.id, formData).subscribe((data: any) => {
        this.loader = false;
        this.toastr.success(data.message);
        this.dialogRef.close();
      });
    } else {
      this.loader = true;
      let formData = new FormData();
      formData.append('fullName', this.tags.fullName);
      formData.append('photo', this.selectedFile);
      formData.append('type', this.tags.type);

      this.api.createTeamMember(formData).subscribe((data: any) => {
        this.loader = false;
        this.toastr.success(data.message);
        this.dialogRef.close();
        this.getTeamMember();
      });
    }
  }

  getTeamMember() {
    this.api.getTeamMember().subscribe((data: any) => {
      this.tags = data;
      console.log(this.tags);
    });
  }
}
