import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { TagsFormComponent } from './tags-form/tags-form.component';
import {AdminApiService} from "../../../../core/http/adminApi/admin-api.service";

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {


  displayedColumns: string[] = [ 'ID','TagName','PostIdList','Actions'];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private api:AdminApiService, public dialog: MatDialog, private toastr: ToastrService) { }

  categoryitem:any = {};

  dtOptions: DataTables.Settings = {};

  openTagsDialog(){
    const dialogRef = this.dialog.open(TagsFormComponent, {
      width: '500px',

      disableClose:true,

    })
  }

  openUpdateTagsDialog(id:any){
    const dialogRef = this.dialog.open(TagsFormComponent, {
      width: '500px',
      disableClose:true,
      data:id,

    })
  }


  ngOnInit(): void {

    this.api.getRefreshNeeded().subscribe(()=>{
      this.tagsList();
    })
    this.tagsList();

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: false,
      lengthMenu: [5, 10, 25, 50, 100],




    }

  }





  tagsList(){
    this.api.getProducts().subscribe((data:any)=>{
      this.dataSource = new MatTableDataSource(data);
      console.log(data)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;


    })
  }

  removeTags(id:any){

    this.api.deleteProduct(id).subscribe((data:any)=>{
      this.toastr.success('Tags Deleted Successfully');

    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
