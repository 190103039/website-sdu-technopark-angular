import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { TeamMemberFormComponent } from './team-member-form/team-member-form.component';
import { AdminApiService } from '../../../../core/http/adminApi/admin-api.service';
import { DataTablesModule } from 'angular-datatables';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-team-member',
  templateUrl: './team-member.component.html',
  styleUrls: ['./team-member.component.css'],
})
export class TeamMembersComponent implements OnInit {
  displayedColumns: string[] = ['ID', 'Type', 'Image', 'FullName', 'Actions'];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  result: any[] = [];
  constructor(
    private api: AdminApiService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer
  ) {}

  categoryitem: any = {};

  dtOptions: DataTables.Settings = {};

  openBannerDialog() {
    const dialogRef = this.dialog.open(TeamMemberFormComponent, {
      width: '500px',
      disableClose: true,
    });
  }

  openUpdateBannerDialog(id: any) {
    const dialogRef = this.dialog.open(TeamMemberFormComponent, {
      width: '500px',
      disableClose: true,
      data: id,
    });
  }

  ngOnInit(): void {
    this.api.getRefreshNeeded().subscribe(() => {
      this.TeamMemberList();
      console.log(this.TeamMemberList);
    });
    this.TeamMemberList();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: false,
      lengthMenu: [5, 10, 25, 50, 100],
    };
  }

  TeamMemberList() {
    this.api.getTeamMember().subscribe((data: any) => {
      this.result = data.map((item: any) => ({
        ...item,
        photo: this.sanitizer.bypassSecurityTrustResourceUrl(
          `data:image/png;base64, ${item.photo}`
        ),
      }));
      this.dataSource = new MatTableDataSource(this.result);
      console.log(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  removeBanner(id: any) {
    let data = { id: id };
    this.api.deleteTeamMember(data).subscribe((data: any) => {
      this.toastr.success('Banner Deleted Successfully');
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
