import { Component, Input, OnInit, Inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminApiService } from '../../../../../core/http/adminApi/admin-api.service';
@Component({
  selector: 'app-employee-contact-form',
  templateUrl: './employee-contact-form.component.html',
  styleUrls: ['./employee-contact-form.component.css'],
})
export class EmployeeContactFormComponent implements OnInit {
  @Input() category: any = {};
  label = 'Create';
  constructor(
    private api: AdminApiService,
    private toastr: ToastrService,
    public router: Router,
    public dialogRef: MatDialogRef<EmployeeContactFormComponent>,
    @Inject(MAT_DIALOG_DATA) public id: any
  ) {}

  selectedFile: any;
  loader: boolean = false;
  img_url = 'http://localhost:4200/assets/no-image-available.png';
  ngOnInit(): void {
    if (this.id) {
      this.label = 'Update';
      let data = {
        id: this.id,
      };
      this.api.employeeGetById(data).subscribe((data: any) => {
        this.category = data;
      //  this.img_url = data.photo;
        console.log(this.category);
      });
    }
    this.getEmployeeContact();
    this.getFile(this.selectedFile);
  }
  getFile(event: any) {
    if (event) {
      this.selectedFile = event.target.files[0];
      this.label = this.selectedFile.name;
      let reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.img_url = event.target.result;
      };
    }
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);
  }

  SaveData() {
    if (this.id) {
      let formData = new FormData();
      formData.append('id', this.id);
      formData.append('fullName', this.category.fullName);
      formData.append('photo', this.selectedFile);
      formData.append('position', this.category.position);
      formData.append('phoneNumber', this.category.phoneNumber);
      formData.append('email', this.category.email);
      this.loader = true;
      this.api.updateEmployeeContact(formData).subscribe((data: any) => {
        this.loader = false;
        this.toastr.success(data.message);
        this.dialogRef.close();
      });
    } else {
      this.loader = true;
      let formData = new FormData();
      formData.append('fullName', this.category.fullName);
      formData.append('photo', this.selectedFile);
      formData.append('position', this.category.position);
      formData.append('phoneNumber', this.category.phoneNumber);
      formData.append('email', this.category.email);

      this.api.createEmployeeContact(formData).subscribe((data: any) => {
        this.loader = false;
        this.toastr.success(data.message);
        this.dialogRef.close();
        this.getEmployeeContact();
      });
    }
  }

  getEmployeeContact() {
    this.api.getEmployees().subscribe((data: any) => {
      this.category = data;
    });
  }
}
