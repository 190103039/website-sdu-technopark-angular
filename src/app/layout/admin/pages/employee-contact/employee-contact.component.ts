import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { EmployeeContactFormComponent } from './employee-contact-form/employee-contact-form.component';
import { AdminApiService } from '../../../../core/http/adminApi/admin-api.service';
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any;

@Component({
  selector: 'app-employee-contact',
  templateUrl: './employee-contact.component.html',
  styleUrls: ['./employee-contact.component.css'],
})
export class EmployeeContactComponent implements OnInit {
  displayedColumns: string[] = [
    'CategoryImage',
    'Name',
    'Position',
    'Phone - Number',
    'email',
    'Actions',
  ];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  result: any[] = [];
  constructor(
    private api: AdminApiService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer
  ) {}
  title = 'Employee Contact';
  categoryitem: any = {};

  dtOptions: DataTables.Settings = {};

  openCategoryDialog() {
    const dialogRef = this.dialog.open(EmployeeContactFormComponent, {
      width: '500px',
      disableClose: false,
    });
  }

  openUpdateCategoryDialog(id: any) {
    const dialogRef = this.dialog.open(EmployeeContactFormComponent, {
      width: '500px',
      disableClose: false,
      data: id,
    });
  }

  ngOnInit(): void {
    this.api.getRefreshNeeded().subscribe(() => {
      this.EmployeesList();
    });
    this.EmployeesList();

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: false,
      lengthMenu: [5, 10, 25, 50, 100],
    };
  }
  EmployeesList() {
    this.api.getEmployees().subscribe((data: any) => {
      this.result = data.map((item: any) => ({
        ...item,
        photo: this.sanitizer.bypassSecurityTrustResourceUrl(
          `data:image/png;base64, ${item.photo}`
        ),
      }));
      console.log(data);
      this.dataSource = new MatTableDataSource(this.result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  removeEmployee(id: any) {
    let data = { id: id };
    console.log(data);
    this.api.deleteEmployeeContact(data.id).subscribe((data: any) => {
      this.toastr.success('Employee Deleted Successfully');
      this.EmployeesList();
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
