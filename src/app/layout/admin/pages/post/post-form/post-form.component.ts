import {Component, Input, OnInit, Inject} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {AdminApiService} from "../../../../../core/http/adminApi/admin-api.service";

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  @Input() category: any = {};
  label = "Create";

  constructor(private api: AdminApiService, private toastr: ToastrService, public router: Router, public dialogRef: MatDialogRef<PostFormComponent>, @Inject(MAT_DIALOG_DATA) public id: any) {
  }

  selectedFile: any;
  loader: boolean = false;
  img_url = 'http://localhost:4200/assets/no-image-available.png';

  ngOnInit(): void {
    if (this.id) {
      this.label = "Update";
      let data = {
        "id": this.id
      }
      this.api.postGetById(data).subscribe((data: any) => {
        this.category = data.data;
        this.img_url = data.data.image_url;
        console.log(this.category);
      })
    }
    this.getCategory();
    this.getFile(this.selectedFile);

  }

  getFile(event: any) {
    if (event) {
      this.selectedFile = event.target.files[0];
      this.label = this.selectedFile.name;
      let reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.img_url = event.target.result;
      };
    }
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);
  }


  SaveData() {

    if (this.id) {

      let formData = new FormData();
      formData.append('id', this.id);
      formData.append('name', this.category.name);
      formData.append('img_url', this.selectedFile);
      this.loader = true;
      this.api.updatePost(formData).subscribe((data: any) => {
        this.loader = false;
        this.toastr.success(data.message);
        this.dialogRef.close();


      })
    } else {
      this.loader = true;
      let formData = new FormData();
      formData.append('name', this.category.name);
      formData.append('img_url', this.selectedFile);


      this.api.createPost(formData).subscribe((data: any) => {
        this.loader = false;
        this.toastr.success(data.message);
        this.dialogRef.close();
        this.getCategory();

      })
    }


  }

  getCategory() {
    this.api.getPosts().subscribe((data: any) => {
      this.category = data.data;
    })
  }


}
