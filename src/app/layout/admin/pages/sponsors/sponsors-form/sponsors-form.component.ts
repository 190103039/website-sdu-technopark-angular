import { Component, Input, OnInit, Inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminApiService } from '../../../../../core/http/adminApi/admin-api.service';
@Component({
  selector: 'app-sponsors-form',
  templateUrl: './sponsors-form.component.html',
  styleUrls: ['./sponsors-form.component.css'],
})
export class SponsorsFormComponent implements OnInit {
  @Input() category: any = {};
  label = 'Create';
  constructor(
    private api: AdminApiService,
    private toastr: ToastrService,
    public router: Router,
    public dialogRef: MatDialogRef<SponsorsFormComponent>,
    @Inject(MAT_DIALOG_DATA) public id: any
  ) {}

  selectedFile: any;
  loader: boolean = false;
  img_url = 'http://localhost:4200/assets/no-image-available.png';
  ngOnInit(): void {
    if (this.id) {
      this.label = 'Update';
      let data = {
        id: this.id,
      };
      this.api.sponsorsGetById(data).subscribe((data: any) => {
        this.category = data;
        console.log(this.category);
      });
    }
    this.getSponsors();
    this.getFile(this.selectedFile);
  }
  getFile(event: any) {
    if (event) {
      this.selectedFile = event.target.files[0];
      this.label = this.selectedFile.name;
      let reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.img_url = event.target.result;
      };
    }
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);
  }

  SaveData() {
    if (this.id) {
      let formData = new FormData();
      formData.append('id', this.id);
      formData.append('sponsorName', this.category.sponsorName);
      formData.append('logo', this.selectedFile);
      this.loader = true;
      this.api.updatePost(formData).subscribe((data: any) => {
        this.loader = false;
        this.toastr.success(data.message);
        this.dialogRef.close();
      });
    } else {
      this.loader = true;
      let formData = new FormData();
      formData.append('sponsorName', this.category.sponsorName);
      formData.append('logo', this.selectedFile);

      this.api.createPost(formData).subscribe((data: any) => {
        this.loader = false;
        this.toastr.success(data.message);
        this.dialogRef.close();
        this.getSponsors();
      });
    }
  }

  getSponsors() {
    this.api.getSponsors().subscribe((data: any) => {
      this.category = data;
    });
  }
}
