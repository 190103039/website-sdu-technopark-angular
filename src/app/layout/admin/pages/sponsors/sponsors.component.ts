import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ToastrService} from 'ngx-toastr';
import {SponsorsFormComponent} from './sponsors-form/sponsors-form.component';
import {AdminApiService} from "../../../../core/http/adminApi/admin-api.service";
import { DomSanitizer } from '@angular/platform-browser';

declare var $: any;



@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.css']
})
export class SponsorsComponent implements OnInit  {

  displayedColumns: string[] = [ 'Logo','Name','Actions'];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  result: any[] = [];
  constructor(private api:AdminApiService, public dialog: MatDialog, private toastr: ToastrService,private sanitizer: DomSanitizer) { }
  title = 'Sponsors';
  categoryitem:any = {};

  dtOptions: DataTables.Settings = {};

  openSponsorsDialog(){
    const dialogRef = this.dialog.open(SponsorsFormComponent, {
      width: '350px',
      disableClose:true,
    })
  }

  openUpdateSponsorsDialog(id:any){
    const dialogRef = this.dialog.open(SponsorsFormComponent, {
      width: '500px',
      disableClose:true,
      data:id,
    })
  }


  ngOnInit(): void {
    this.api.getRefreshNeeded().subscribe(()=>{
      this.SponsorsList();
    })
    this.SponsorsList();

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: false,
      lengthMenu: [5, 10, 25, 50, 100],

    }

  }

 SponsorsList(){
    this.api.getSponsors().subscribe((data:any)=>{
      this.result = data.map((item: any) => ({
        ...item,
        logo: this.sanitizer.bypassSecurityTrustResourceUrl(
          `data:image/png;base64, ${item.logo}`
        ),
      }));
      this.dataSource = new MatTableDataSource(this.result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;


    })
  }

  removeSponsors(id:any){
    let data={"id":id}
    this.api.deleteSponsors(data).subscribe((data:any)=>{
      this.toastr.success('Sponsor Deleted Successfully');
      this.SponsorsList();
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
