import { RouterModule, Routes } from "@angular/router";
import {
  TagsComponent,
  PostComponent,
  OrderComponent,
  DashboardComponent,
  SponsorsComponent,
  EmployeeContactComponent,
  TeamMembersComponent,
} from './index';
import { NgModule } from "@angular/core";
import { AdminPageComponent } from "./admin-main-layout/admin-page.component";

export const routes: Routes = [
  {
    path: '', redirectTo: '/admin', pathMatch: 'full',
  }, {
    path: 'admin', component: AdminPageComponent,
  },
  {
    path: 'dashboard', component: DashboardComponent,
  },
  {
    path: 'tags', component: TagsComponent
  },
  {
    path: 'team-members', component: TeamMembersComponent
  },
  {
    path: 'post', component: PostComponent
  },
  {
    path: 'order', component: OrderComponent
  },
  {
    path: 'employee', component: EmployeeContactComponent
  },
  {
    path: 'sponsors', component: SponsorsComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRouterModule {
}
