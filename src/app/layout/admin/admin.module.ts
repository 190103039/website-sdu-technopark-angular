import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { AutoCompleteModule } from 'angular-ngx-autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { NgChartsModule } from 'ng2-charts';
import { ChartModule } from 'angular2-chartjs';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { TagsComponent } from './pages/tags/tags.component';
import { TeamMembersComponent } from './pages/team-member/team-member.component';
import { PostComponent } from './pages/post/post.component';
import { OrderComponent } from './pages/order/order.component';
import { EmployeeContactComponent } from './pages/employee-contact/employee-contact.component';
import { SponsorsComponent } from './pages/sponsors/sponsors.component';
import { TeamMemberFormComponent } from './pages/team-member/team-member-form/team-member-form.component';
import { PostFormComponent } from './pages/post/post-form/post-form.component';
import { TagsFormComponent } from './pages/tags/tags-form/tags-form.component';
import { EmployeeContactFormComponent } from './pages/employee-contact/employee-contact-form/employee-contact-form.component';
import { SponsorsFormComponent } from './pages/sponsors/sponsors-form/sponsors-form.component';
import { AdminPageComponent } from './admin-main-layout/admin-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { AdminRouterModule } from './admin-router.module';

@NgModule({
  declarations: [
    DashboardComponent,
    TagsComponent,
    TeamMembersComponent,
    PostComponent,
    OrderComponent,
    TagsFormComponent,
    PostFormComponent,
    TeamMemberFormComponent,
    EmployeeContactComponent,
    EmployeeContactFormComponent,
    SponsorsComponent,
    SponsorsFormComponent,
    AdminPageComponent,
  ],
  imports: [
    AdminRouterModule,
    TranslateModule,
    CommonModule,
    HttpClientModule,
    DataTablesModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatDialogModule,
    FormsModule,
    AutoCompleteModule,
    MatSelectModule,
    MatAutocompleteModule,
    NgChartsModule,
    SweetAlert2Module.forRoot(),
    ChartModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      timeOut: 1500,
    }),
  ],
  bootstrap: [AdminPageComponent],
})
export class AdminModule {}
