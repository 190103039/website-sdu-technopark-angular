import { Image } from "./image-interface"

export interface TextLink {
    text: string,
    url: string | null,
    file: Image | null
}

export interface TextInfoLink {
    title: string,
    url: string | null,
    info: string | null,
    file: Image | null
}