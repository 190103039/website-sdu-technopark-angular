import {NgModule} from "@angular/core";

import {CommonModule, NgOptimizedImage} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AngularSvgIconModule} from "angular-svg-icon";
import {RouterModule} from "@angular/router";
import { CarouselComponent, IvyCarouselModule } from "angular-responsive-carousel";
import {CarouselsComponent} from "./components/carousel/carousel.component";
import { NewsCarouselComponent } from "./components/news-carousel/news-carousel.component";
import {MatTabsModule} from "@angular/material/tabs";
import {TranslateModule} from "@ngx-translate/core";

const components =[
  CarouselsComponent,
  NewsCarouselComponent
]
@NgModule({
  declarations: [...components],

  imports: [
    CommonModule,
    RouterModule,
    AngularSvgIconModule,
    ReactiveFormsModule,
    FormsModule,
    MatTabsModule,
    TranslateModule,
  ],
  exports: [...components],
})
export class SharedModule {
}
