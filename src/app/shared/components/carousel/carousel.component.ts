import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  Input,
  Renderer2,
  NgZone,
} from '@angular/core';
import { TextLink } from '../../interfaces/text-interface';

interface CarouselItem {
  element: HTMLElement;
  translate: number;
}

type Direction = 'backward' | 'forward';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselsComponent implements OnInit, AfterViewInit {
  @Input() link!: TextLink;
  @ViewChild('scroll') scroll!: ElementRef;
  public carouselWrapper!: any;
  public isControlDisabled: boolean = false;

  margin: number = 24;

  translatePercent: number = 0;
  translatePx: number = 0;
  activeIndex: number = 0;
  carouselCounter: number = 0;

  firstElementIndex: number = 0;
  lastElementIndex: number = 0;

  buttonTimeout = 500;

  carouselItems: CarouselItem[] = [];

  constructor(private renderer: Renderer2, private zone: NgZone) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.carouselWrapper = this.scroll.nativeElement;
    let items = this.carouselWrapper.children;

    for (let item of items) {
      this.carouselItems.push({
        element: item,
        translate: 0,
      });
    }

    this.initCarousel();
  }

  initCarousel() {
    this.zone.runOutsideAngular(() => {
      this.renderer.setStyle(
        this.carouselWrapper,
        'transform',
        `translateX(${this.translatePercent}%) translateX(${this.translatePx}px)`
      );

      let itemsCount = this.carouselItems.length;
      console.log(itemsCount);

      if (itemsCount >= 3) {
        this.firstElementIndex = itemsCount - 1;
        this.lastElementIndex = this.firstElementIndex - 1;

        let firstItem = this.carouselItems[this.firstElementIndex];
        firstItem.translate = itemsCount * -100;

        this.renderer.setStyle(
          firstItem.element,
          'transform',
          `translateX(${firstItem.translate}%)`
        );
      }
      else if (itemsCount === 2) {
        this.firstElementIndex = 0;
        this.lastElementIndex = itemsCount - 1;
      }
    });
  }

  moveCarousel(direction: Direction) {
    this.zone.runOutsideAngular(() => {
      if (this.isControlDisabled) {
        return;
      }

      this.triggerControl();

      switch (direction) {
        case 'backward':
          this.moveBackward();
          break;
        case 'forward':
          this.moveForward();
          break;
        default:
          break;
      }

      setTimeout(() => this.triggerControl(), this.buttonTimeout);
    });
  }

  moveBackward() {
    let itemsCount = this.carouselItems.length;
    this.carouselCounter++;

    this.translatePercent = this.carouselCounter * 100;
    this.translatePx = this.carouselCounter * this.margin;

    this.activeIndex =
      this.activeIndex > 0 ? this.activeIndex - 1 : itemsCount - 1;

    if (this.activeIndex === this.firstElementIndex && itemsCount >= 3) {
      this.moveLastElementToStart();

      if (itemsCount >= 5) {
        this.moveLastElementToStart();
      }
    }
    else if (this.activeIndex === this.lastElementIndex) {
      this.moveLastElementToStart();
    }

    this.renderer.setStyle(
      this.carouselWrapper,
      'transform',
      `translateX(${this.translatePercent}%) translateX(${this.translatePx}px)`
    );
  }

  moveForward() {
    let itemsCount = this.carouselItems.length;
    this.carouselCounter--;

    this.translatePercent = this.carouselCounter * 100;
    this.translatePx = this.carouselCounter * this.margin;

    this.activeIndex =
      this.activeIndex < itemsCount - 1 ? this.activeIndex + 1 : 0;

    if (this.activeIndex === this.lastElementIndex && itemsCount >= 3) {
      this.moveFirstElementToEnd();

      if (itemsCount >= 5) {
        this.moveFirstElementToEnd();
      }
    }
    else if (this.activeIndex === this.firstElementIndex) {
      console.log('test');
      this.moveFirstElementToEnd();
    }

    this.renderer.setStyle(
      this.carouselWrapper,
      'transform',
      `translateX(${this.translatePercent}%) translateX(${this.translatePx}px)`
    );
  }

  triggerControl() {
    this.isControlDisabled = !this.isControlDisabled;
  }

  moveLastElementToStart() {
    let itemsCount = this.carouselItems.length;

    let currentItem = this.carouselItems[this.lastElementIndex];
    currentItem.translate -= itemsCount * 100;

    this.renderer.setStyle(
      currentItem.element,
      'transform',
      `translateX(${currentItem.translate}%)`
    );

    this.firstElementIndex = this.lastElementIndex;
    this.lastElementIndex =
      this.lastElementIndex === 0 ? itemsCount - 1 : this.lastElementIndex - 1;
  }

  moveFirstElementToEnd() {
    let itemsCount = this.carouselItems.length;

    let currentItem = this.carouselItems[this.firstElementIndex];
    currentItem.translate += itemsCount * 100;

    this.renderer.setStyle(
      currentItem.element,
      'transform',
      `translateX(${currentItem.translate}%)`
    );

    this.lastElementIndex = this.firstElementIndex;
    this.firstElementIndex =
      this.firstElementIndex === itemsCount - 1
        ? 0
        : this.firstElementIndex + 1;
  }

  moveToIndex(nextIndex: number) {
    this.zone.runOutsideAngular(() => {
      if (nextIndex === this.activeIndex) {
        return;
      }

      let forwardDistance =
        nextIndex > this.activeIndex
          ? nextIndex - this.activeIndex
          : this.carouselItems.length - this.activeIndex + nextIndex;
      let backwardDistance =
        nextIndex > this.activeIndex
          ? this.carouselItems.length - nextIndex + this.activeIndex
          : this.activeIndex - nextIndex;

      if (forwardDistance <= backwardDistance) {
        for (let i = 0; i < forwardDistance; i++) {
          this.moveForward();
        }
      } else {
        for (let i = 0; i < backwardDistance; i++) {
          this.moveBackward();
        }
      }
    });
  }
}
