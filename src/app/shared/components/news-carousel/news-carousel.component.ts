import { Component, OnInit, Input } from '@angular/core';

import { News } from '../../interfaces/news-interface';
import { TextLink } from '../../interfaces/text-interface';
import { HttpService } from 'src/app/core/http/http.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-news-carousel',
  templateUrl: './news-carousel.component.html',
  styleUrls: ['./news-carousel.component.scss'],
})
export class NewsCarouselComponent implements OnInit {
  students: any[] = [];
  teachers: any[] = [];
  result: any[] = [];
  constructor(
    private httpClient: HttpService,
    private sanitizer: DomSanitizer
  ) {}

  @Input() newsMatrix: News[][] = [];
  @Input() newsMatrix1: News[][] = [];
  @Input() link: TextLink = {
    text: 'Посмотреть все',
    url: '',
    file: null,
  };

  @Input() padding: string = '40px 0';
  @Input() backgroundColor: string = '#F1F3F4';
  @Input() itemHeight: string = '336px';

  ngOnInit() {
    const chunkSize = 4;
    this.httpClient
      .getMethod('api/v1/team-info/list')
      .subscribe((data: any) => {
        this.students = data
          .filter((item: any) => item.type === 'STUDENT')
          .map((item: any) => ({
            ...item,
            photo: this.sanitizer.bypassSecurityTrustResourceUrl(
              `data:image/png;base64, ${item.photo}`
            ),
          }));

        console.log(this.students);

        for (let i = 0; i < this.students.length; i += chunkSize) {
          const chunk = this.students.slice(i, i + chunkSize);
          chunk.map((item: any) => ({
            ...item,
            photo: this.sanitizer.bypassSecurityTrustResourceUrl(
              `data:image/png;base64, ${item.photo}`
            ),
          }));
          console.log(chunk);
          this.newsMatrix.push(chunk);
          // this.result.push(chunk);
        }

        console.log(this.newsMatrix);
        this.teachers = data
          .filter((item: any) => item.type === 'EMPLOYEE')
          .map((item: any) => ({
            ...item,
            photo: this.sanitizer.bypassSecurityTrustResourceUrl(
              `data:image/png;base64, ${item.photo}`
            ),
          }));
          for (let i = 0; i < this.teachers.length; i += chunkSize) {
            const chunk = this.teachers.slice(i, i + chunkSize);
            chunk.map((item: any) => ({
              ...item,
              photo: this.sanitizer.bypassSecurityTrustResourceUrl(
                `data:image/png;base64, ${item.photo}`
              ),
            }));
            console.log(chunk);
            this.newsMatrix1.push(chunk);
            console.log(this.newsMatrix1);
            // this.result.push(chunk);
          }
      });



  }
}
