import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ClientModule} from "./layout/client/client.module";
import {AdminModule} from "./layout/admin/admin.module";
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { NgxSpinnerModule } from 'ngx-spinner';
import {SharedModule} from "./shared/shared.module";
import { AngularSvgIconModule } from 'angular-svg-icon';
import { GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from '@abacritt/angularx-social-login';
import { JwtModule } from '@auth0/angular-jwt';
import { GoogleSigninButtonDirective } from "@abacritt/angularx-social-login";
import { AuthGuardService } from './auth/_services/auth_guard_service';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CarouselModule.forRoot(),
    BrowserAnimationsModule,
    ClientModule,
    AdminModule,
    SharedModule,
    SocialLoginModule,
    AngularSvgIconModule.forRoot(),
    NgxSpinnerModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['your-api-domain'], // Replace with your API domain or leave blank for all domains
        disallowedRoutes: ['your-api-url/auth/login'], // Replace with your login endpoint or leave blank to allow all routes
      },
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider('459773865241-s26a0bat2sp0mre6hmdqf7hurmvd6boq.apps.googleusercontent.com'),
          },
        ],
      } as SocialAuthServiceConfig,
    },
    
],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
