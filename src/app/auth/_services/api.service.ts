import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  // Replace with your API endpoint URL

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private jwtHelper: JwtHelperService,
    private token: StorageService
  ) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    '  Authorization': 'Bearer ' + localStorage.getItem('token'),
    }),
  };

  createUser(userData: any): Promise<any> {
    return this.http
      .post('/api/authenticate', userData, this.httpOptions)
      .toPromise();
  }
  getToken(): string {
    //@ts-ignore
    // const token = this.token.getToken();
    return localStorage.getItem('token');
  }

  setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  isLoggedIn(): boolean {
    const token = this.getToken();
    //@ts-ignore
    return token && !this.jwtHelper.isTokenExpired(token);
  }
}
