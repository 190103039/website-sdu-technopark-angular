import { GoogleLoginProvider, SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class GoogleAuthService {
   user!: SocialUser;

  constructor(private authService: SocialAuthService) {}

    signIn(): Promise<SocialUser> {
      return this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((user: SocialUser) => {
        console.log(user)
        this.user = user;
        return user;
      });
  }

  signOut(): Promise<void> {
    return this.authService.signOut()
      .then(() => {
        //@ts-ignore
        this.user = null;
      });
  }

  getUser(): SocialUser {
    return this.user;
  }
}
