import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  public postMethod(url: string, body: any): Observable<any> {
    return this.http.post(url, body)
  }

  public getMethod(url: string): Observable<any> {
    return this.http.get(url);
  }



}
