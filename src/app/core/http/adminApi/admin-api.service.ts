import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminApiService {

  constructor(private http: HttpClient) {
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('adminToken')
    }),
  };

  postHttpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('adminToken')
    })
  };


  private refreshNeeded$ = new Subject<void>();

  getRefreshNeeded() {
    return this.refreshNeeded$;
  }

  /* == Tags == */

  getProducts(): Observable<any> {
    return this.http.get('/api/v1/post/tags/list', this.httpOptions);
  }

  createProduct(product: FormData): Observable<any> {
    return this.http.post<string>('/api/v1/post/tags', product, this.postHttpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  getProductById(id: any): Observable<any> {
    return this.http.get('/api/v1/post/tags?id=' + id, this.httpOptions);
  }

  updateProduct(product: any): Observable<any> {
    return this.http.post('/api/v1/post/tags', product, this.postHttpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  deleteProduct(id: any): Observable<any> {
    return this.http.get('products/delete?id=' + id, this.httpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  /* == Team-member == */

  getTeamMember(): Observable<any> {
    return this.http.get('/api/v1/team-info/list', this.httpOptions);
  }

  TeamMemberById(id: any): Observable<any> {
    return this.http.get('/api/v1/team-info/' + id.id);
  }

  createTeamMember(category: FormData): Observable<any> {
    console.log(category);
    return this.http.post<any>('/api/v1/team-info', category, this.postHttpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  updateTeamMember(id: any, body:FormData): Observable<any> {
    return this.http.put('/api/v1/team-info/' + id, body).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  deleteTeamMember(id: any): Observable<any> {
    return this.http.delete('/api/v1/team-info/' +id.id).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  /* == Posts == */

  getPosts(): Observable<any> {
    return this.http.get('/api/v1/posts/list', this.httpOptions);
  }

  postGetById(id: any): Observable<any> {
    return this.http.get('/api/v1/posts/' + id.id);
  }

  createPost(post: FormData): Observable<any> {
    console.log(post);
    return this.http.post<string>('/api/v1/posts', post, this.postHttpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  updatePost(post: FormData): Observable<any> {
    return this.http.post<string>('category/update', post, this.postHttpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  deletePost(id: any): Observable<any> {
    return this.http.delete('/api/v1/posts/'+id.id).pipe(tap(() => {
      this.refreshNeeded$.next();
    }),);
  }


  /* == Order == */

  getOrders(): Observable<any> {
    return this.http.post('orders/list', {}, this.httpOptions);
  }


  /*Vendor */
  getVendors(): Observable<any> {
    return this.http.get('vendor/list', this.httpOptions);

  }


  /* == User == */


  /* == Dashboard == */

  getDashboard(): Observable<any> {
    return this.http.post('admin/dashboard-count', {}, this.httpOptions);
  }

  getMonthlySales(data: any): Observable<any> {
    return this.http.post('admin/monthly_sales', data, this.httpOptions);
  }

  getSalesInsights(): Observable<any> {
    return this.http.post('admin/sales-insights', {}, this.httpOptions);
  }


  /* == Login == */

  adminLogin(user: any): Observable<any> {
    return this.http.post('admin/signin', user, this.httpOptions);
  }


  /* == EmployeeContact == */

  getEmployees(): Observable<any> {
    return this.http.get('/api/v1/contacts', this.httpOptions);
  }

  employeeGetById(id: any): Observable<any> {
    return this.http.get('/api/v1/contacts/' + id.id);
  }

  createEmployeeContact(category: FormData): Observable<any> {
    return this.http.post<string>('/api/v1/contacts', category, this.postHttpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  //TODO need to add update ti UI
  updateEmployeeContact(category: FormData): Observable<any> {
    return this.http.post<string>('/api/v1/contacts', category, this.postHttpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  deleteEmployeeContact(id: any): Observable<any> {
    return this.http.delete('/api/v1/contacts?id=' + id, this.httpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }),);
  }


  /* == Sponsors == */

  getSponsors(): Observable<any> {
    return this.http.get('/api/v1/sponsors', this.httpOptions);
  }

  sponsorsGetById(id: any): Observable<any> {
    return this.http.get('/api/v1/sponsors?id=' + id, this.httpOptions);
  }

  createSponsors(category: FormData): Observable<any> {
    return this.http.post<string>('/api/v1/sponsors', category, this.postHttpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  updateSponsors(category: FormData): Observable<any> {
    return this.http.post<string>('/api/v1/contacts', category, this.postHttpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }));
  }

  deleteSponsors(id: any): Observable<any> {
    return this.http.delete('/api/v1/sponsors?id=' + id, this.httpOptions).pipe(tap(() => {
      this.refreshNeeded$.next();
    }),);
  }


}
