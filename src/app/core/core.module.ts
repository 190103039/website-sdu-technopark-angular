
import { NgModule } from "@angular/core";
import * as component from './index';
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { AngularSvgIconModule } from "angular-svg-icon";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
@NgModule({
  declarations: [
    component.FooterComponent,
    component.HeaderComponent,
    component.ClientPageComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    AngularSvgIconModule,
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ]
})
export class CoreModule {
}
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
