import {Component} from '@angular/core';
import {HttpService} from '../../http/http.service';

@Component({
  selector: 'app-page',
  templateUrl: './client-page.component.html',
  styleUrls: ['./client-page.component.scss']
})
export class ClientPageComponent {
  myData$: any[] = [];
  constructor(private httpClient: HttpService) { }
  ngOnInit() {
    this.httpClient.getMethod('api/v1/general/website').subscribe((next: any) => {
      this.myData$.push(next)
      console.log(this.myData$)
    })
  }
}
