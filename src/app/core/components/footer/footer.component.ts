import {Component} from '@angular/core';
import {HttpService} from '../../http/http.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  myData$: any[] = [];

  constructor(private httpClient: HttpService) {
  }

  ngOnInit() {
    this.httpClient.getMethod('api/v1/general/website').subscribe((next: any) => {
      this.myData$.push(next)
    })
  }
}
