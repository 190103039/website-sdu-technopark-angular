import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{
  public language: string ='ru';
  myData$:any[] = [];
  constructor(public translate: TranslateService, private httpClient:HttpClient, ) {
    translate.addLangs(['ru','kz','en'])
    translate.setDefaultLang('ru')
    const browserLang = translate.getBrowserLang();
    //@ts-ignore
    translate.use(browserLang.match(/ru|kz/) ? browserLang : 'ru');
  }
  ngOnInit(): void {

  }

}


